import React from "react";
import { StyleSheet, View, Text, Alert } from "react-native";
import { Icon } from "react-native-elements";

import { THEME } from "../theme";

export const Header = () => {
  return (
    <View style={styles.container}>
      <View style={styles.icon}>
        <Icon
          name="bars"
          color="#fff"
          type="font-awesome"
          onPress={() => {
            Alert.alert(
              "Menu",
              "My Alert Msg",
              [
                {
                  text: "Cancel",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel",
                },
                { text: "OK", onPress: () => console.log("OK Pressed") },
              ],
              { cancelable: false }
            );
          }}
        />
      </View>
      <Text style={styles.title}>Potnocne Mazowsze</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: "relative",
    height: 90,
    backgroundColor: THEME.COLOR_1,
    borderBottomEndRadius: 40,
    borderBottomStartRadius: 40,
  },
  title: {
    position: "absolute",
    bottom: 20,
    textAlign: "center",
    width: "100%",
    color: "#fff",
    fontSize: 20,
  },
  icon: {
    position: "absolute",
    bottom: 20,
    left: 30,
    zIndex:2
  },
});
